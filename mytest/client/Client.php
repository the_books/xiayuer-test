<?php

namespace xiayuer\mytest\client;



use xiayuer\mytest\factory\Winxin;
use xiayuer\mytest\factory\Zhifb;

class Client
{
    public static function pay(){
        (new Winxin())->pay();
    }

    public static function format(){
        (new Zhifb())->format();
    }
}