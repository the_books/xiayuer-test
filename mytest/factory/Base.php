<?php

namespace xiayuer\mytest\factory;

abstract class Base
{
    abstract function pay();

    abstract function format();
}